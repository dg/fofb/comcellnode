library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library desyrdl;
use desyrdl.common.all;
use desyrdl.pkg_ccn_ethernet.all;

library ccn_eth_lib;
use ccn_eth_lib.pkg_ccn_ethernet.all;
use ccn_eth_lib.pkg_ccn_ethernet_version.all;

entity top_ccn_ethernet is
    generic(
        G_IPNUM                            : natural :=  0
    );
    port(
        aresetn                     : in std_logic;
        axis_clk                    : out std_logic;
        aclk                        : in std_logic;
        free_100_clk                : in std_logic;

        -- Common Quad signals
        qpll0clk_in                 : in std_logic;
        qpll0refclk_in              : in std_logic;
        qpll0lock_in                : in std_logic;
        qpll0reset_out              : out std_logic;
        gt_powergood                : out std_logic;

        -- PPS input
        pps                         : in std_logic;

        -- AXI-MM Status and Config
        s_axi_m2s                   : in  t_ccn_ethernet_m2s;
        s_axi_s2m                   : out t_ccn_ethernet_s2m;

        -- AXIS TX RX
        s_axis_tx_tvalid            : in std_logic;
        s_axis_tx_tready            : out std_logic;
        s_axis_tx_tdata             : in std_logic_vector(63 downto 0);
        s_axis_tx_tkeep             : in std_logic_vector(7 downto 0);
        s_axis_tx_tlast             : in std_logic;
        s_axis_tx_tuser             : in std_logic;
        m_axis_rx_tvalid            : out std_logic;
        m_axis_rx_tuser             : out std_logic;
        m_axis_rx_tlast             : out std_logic;
        m_axis_rx_tkeep             : out std_logic_vector(7 downto 0);
        m_axis_rx_tdata             : out std_logic_vector(63 downto 0);

        -- SFP signals
        sfp_rxn                     : in std_logic;
        sfp_rxp                     : in std_logic;
        sfp_txn                     : out std_logic;
        sfp_txp                     : out std_logic;
        sfp_rx_los                  : in std_logic;
        sfp_mod_abs                 : in std_logic;
        sfp_tx_disable              : out std_logic;
        sfp_tx_fault                : in std_logic

    );
end entity top_ccn_ethernet;

architecture rtl of top_ccn_ethernet is

    ------------------------
    -- SIGNAL DECLARATION --
    ------------------------
    signal addrmap_i           : t_addrmap_ccn_ethernet_in;
    signal addrmap_o           : t_addrmap_ccn_ethernet_out;
    signal areset              : std_logic;
    signal s_gt_powergood      : std_logic;

begin

    areset <= not aresetn;

    ----------------------
    -- AXI-MM INTERFACE --
    ----------------------
    inst_aximm: entity desyrdl.ccn_ethernet
    port map(
        pi_clock    => aclk,
        pi_reset    => areset,
        pi_s_top    => s_axi_m2s,
        po_s_top    => s_axi_s2m,
        pi_addrmap  => addrmap_i,
        po_addrmap  => addrmap_o
    );

    addrmap_i.version.data.data  <= C_VERSION;

    addrmap_i.gt_status.powergood.data(0) <= s_gt_powergood;
    gt_powergood                          <= s_gt_powergood;

    --------------
    -- ETHERNET --
    --------------
    inst_ethernet:entity ccn_eth_lib.ccn_ethernet
    generic map(G_IPNUM => G_IPNUM)
    port map(
        free_100_clk                       => free_100_clk,
        axi_clk                            => aclk,
        axis_clk                           => axis_clk,
        rst_n                              => aresetn,

        qpll0clk_in                        => qpll0clk_in,
        qpll0refclk_in                     => qpll0refclk_in,
        qpll0lock_in                       => qpll0lock_in,
        qpll0reset_out                     => qpll0reset_out,

        tx_clk_rst                         => addrmap_o.gt_control.rst_tx_clk.data(0),
        rx_clk_rst                         => addrmap_o.gt_control.rst_rx_clk.data(0),
        tx_data_rst                        => addrmap_o.gt_control.rst_tx_data.data(0),
        rx_data_rst                        => addrmap_o.gt_control.rst_rx_data.data(0),
        tx_rst                             => addrmap_o.gt_control.rst_tx.data(0),
        rx_rst                             => addrmap_o.gt_control.rst_rx.data(0),
        gt_loopback                        => addrmap_o.gt_control.loopback.data,
        gt_rx_out_clk                      => open,
        gt_tx_out_clk                      => open,

        -- SFP signals
        sfp_rxn                            => sfp_rxn,
        sfp_rxp                            => sfp_rxp,
        sfp_txn                            => sfp_txn,
        sfp_txp                            => sfp_txp,
        sfp_rx_los                         => sfp_rx_los,
        sfp_mod_abs                        => sfp_mod_abs,
        sfp_tx_disable                     => sfp_tx_disable,
        sfp_tx_fault                       => sfp_tx_fault,

        -- Status
        pm_tick                            => pps,
        tx_clk_active                      => addrmap_i.gt_status.tx_clk_active.data(0),
        rx_clk_active                      => addrmap_i.gt_status.rx_clk_active.data(0),
        gt_powergood                       => s_gt_powergood,
        cdr_stable                         => addrmap_i.gt_status.cdr_stable.data(0),
        rx_los                             => addrmap_i.gt_status.sfp_rx_los.data(0),
        mod_abs                            => addrmap_i.gt_status.sfp_mod_abs.data(0),
        tx_fault                           => addrmap_i.gt_status.sfp_tx_fault.data(0),

        -- AXIS
        rx_axis_tvalid                     => m_axis_rx_tvalid,
        rx_axis_tdata                      => m_axis_rx_tdata,
        rx_axis_tlast                      => m_axis_rx_tlast,
        rx_axis_tkeep                      => m_axis_rx_tkeep,
        rx_axis_tuser                      => m_axis_rx_tuser,
        tx_axis_tready                     => s_axis_tx_tready,
        tx_axis_tvalid                     => s_axis_tx_tvalid,
        tx_axis_tdata                      => s_axis_tx_tdata,
        tx_axis_tlast                      => s_axis_tx_tlast,
        tx_axis_tkeep                      => s_axis_tx_tkeep,
        tx_axis_tuser                      => s_axis_tx_tuser,

        -- AXI MM
        s_axi_awaddr                       => addrmap_o.eth.awaddr(11 downto 0),
        s_axi_awvalid                      => addrmap_o.eth.awvalid,
        s_axi_awready                      => addrmap_i.eth.awready,
        s_axi_wdata                        => addrmap_o.eth.wdata,
        s_axi_wstrb                        => addrmap_o.eth.wstrb,
        s_axi_wvalid                       => addrmap_o.eth.wvalid,
        s_axi_wready                       => addrmap_i.eth.wready,
        s_axi_bresp                        => addrmap_i.eth.bresp,
        s_axi_bvalid                       => addrmap_i.eth.bvalid,
        s_axi_bready                       => addrmap_o.eth.bready,
        s_axi_araddr                       => addrmap_o.eth.araddr(11 DOWNTO 0),
        s_axi_arvalid                      => addrmap_o.eth.arvalid,
        s_axi_arready                      => addrmap_i.eth.arready,
        s_axi_rdata                        => addrmap_i.eth.rdata,
        s_axi_rresp                        => addrmap_i.eth.rresp,
        s_axi_rvalid                       => addrmap_i.eth.rvalid,
        s_axi_rready                       => addrmap_o.eth.rready

        );


end architecture;
