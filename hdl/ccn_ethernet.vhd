library ieee;
use ieee.std_logic_1164.all;

library ccn_eth_lib;
use ccn_eth_lib.pkg_ccn_ethernet.all;

entity ccn_ethernet is
    generic(
        G_IPNUM                            : natural :=  0
    );
    port(
        free_100_clk                       : in std_logic;
        axi_clk                            : in std_logic;
        axis_clk                           : out std_logic;
        rst_n                              : in std_logic;

        qpll0clk_in                        : in std_logic;
        qpll0refclk_in                     : in std_logic;
        qpll0lock_in                       : in std_logic;
        qpll0reset_out                     : out std_logic;

        tx_clk_rst                         : in std_logic;
        rx_clk_rst                         : in std_logic;
        tx_data_rst                        : in std_logic;
        rx_data_rst                        : in std_logic;
        tx_rst                             : in std_logic;
        rx_rst                             : in std_logic;
        gt_loopback                        : in std_logic_vector(2 downto 0);
        gt_rx_out_clk                      : out std_logic;
        gt_tx_out_clk                      : out std_logic;

        -- SFP signals
        sfp_rxn                            : in std_logic;
        sfp_rxp                            : in std_logic;
        sfp_txn                            : out std_logic;
        sfp_txp                            : out std_logic;
        sfp_rx_los                         : in std_logic;
        sfp_mod_abs                        : in std_logic;
        sfp_tx_disable                     : out std_logic;
        sfp_tx_fault                       : in std_logic;

        -- Status
        pm_tick                            : in std_logic;
        tx_clk_active                      : out std_logic;
        rx_clk_active                      : out std_logic;
        gt_powergood                       : out std_logic;
        cdr_stable                         : out std_logic;
        rx_los                             : out std_logic;
        mod_abs                            : out std_logic;
        tx_fault                           : out std_logic;

        -- AXIS
        rx_axis_tvalid                     : OUT STD_LOGIC;
        rx_axis_tdata                      : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
        rx_axis_tlast                      : OUT STD_LOGIC;
        rx_axis_tkeep                      : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        rx_axis_tuser                      : OUT STD_LOGIC;
        tx_axis_tready                     : OUT STD_LOGIC;
        tx_axis_tvalid                     : IN STD_LOGIC;
        tx_axis_tdata                      : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        tx_axis_tlast                      : IN STD_LOGIC;
        tx_axis_tkeep                      : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        tx_axis_tuser                      : IN STD_LOGIC;

        -- AXI MM
        s_axi_awaddr                       : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
        s_axi_awvalid                      : IN STD_LOGIC;
        s_axi_awready                      : OUT STD_LOGIC;
        s_axi_wdata                        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_wstrb                        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_wvalid                       : IN STD_LOGIC;
        s_axi_wready                       : OUT STD_LOGIC;
        s_axi_bresp                        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_bvalid                       : OUT STD_LOGIC;
        s_axi_bready                       : IN STD_LOGIC;
        s_axi_araddr                       : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
        s_axi_arvalid                      : IN STD_LOGIC;
        s_axi_arready                      : OUT STD_LOGIC;
        s_axi_rdata                        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_rresp                        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_rvalid                       : OUT STD_LOGIC;
        s_axi_rready                       : IN STD_LOGIC

        );
end entity ccn_ethernet;

architecture struct of ccn_ethernet is

    --------------------------
    -- INTERFACE ATTRIBUTES --
    --------------------------
    ATTRIBUTE X_INTERFACE_INFO                       : STRING;
    ATTRIBUTE X_INTERFACE_PARAMETER                  : STRING;

    ATTRIBUTE X_INTERFACE_INFO of rst_n              :  SIGNAL is "xilinx.com:signal:reset:1.0 rst_n RST";
    ATTRIBUTE X_INTERFACE_PARAMETER of rst_n         :  SIGNAL is "POLARITY ACTIVE_LOW";

    ATTRIBUTE X_INTERFACE_INFO of qpll0reset_out        :  SIGNAL is "xilinx.com:signal:reset:1.0 qpll0reset_out RST";
    ATTRIBUTE X_INTERFACE_PARAMETER of qpll0reset_out   :  SIGNAL is "POLARITY ACTIVE_HIGH";

    ATTRIBUTE X_INTERFACE_PARAMETER of free_100_clk: SIGNAL is "FREQ_HZ 100000000";
    ATTRIBUTE X_INTERFACE_PARAMETER of axi_clk: SIGNAL is "ASSOCIATED_BUSIF s_axi";
    ATTRIBUTE X_INTERFACE_PARAMETER of axis_clk: SIGNAL is "FREQ_HZ 156250000, ASSOCIATED_BUSIF tx_axis:rx_axis";

    ATTRIBUTE X_INTERFACE_INFO of sfp_txn: SIGNAL is "xilinx.com:interface:sfp:1.0 sfp TXN";
    ATTRIBUTE X_INTERFACE_INFO of sfp_rxn: SIGNAL is "xilinx.com:interface:sfp:1.0 sfp RXN";
    ATTRIBUTE X_INTERFACE_INFO of sfp_txp: SIGNAL is "xilinx.com:interface:sfp:1.0 sfp TXP";
    ATTRIBUTE X_INTERFACE_INFO of sfp_rxp: SIGNAL is "xilinx.com:interface:sfp:1.0 sfp RXP";
    ATTRIBUTE X_INTERFACE_INFO of sfp_rx_los: SIGNAL is "xilinx.com:interface:sfp:1.0 sfp RX_LOS";
    ATTRIBUTE X_INTERFACE_INFO of sfp_mod_abs: SIGNAL is "xilinx.com:interface:sfp:1.0 sfp MOD_ABS";
    ATTRIBUTE X_INTERFACE_INFO of sfp_tx_disable: SIGNAL is "xilinx.com:interface:sfp:1.0 sfp TX_DISABLE";
    ATTRIBUTE X_INTERFACE_INFO of sfp_tx_fault: SIGNAL is "xilinx.com:interface:sfp:1.0 sfp TX_FAULT";

    ------------------------
    -- SIGNAL DECLARATION --
    ------------------------
    signal rst              : std_logic;

    signal rxgearboxslip    : std_logic;
    signal rxdatavalid      : std_logic_vector(1 downto 0);
    signal rxheader         : std_logic_vector(5 downto 0);
    signal txheader         : std_logic_vector(5 downto 0);
    signal rxheadervalid    : std_logic_vector(1 downto 0);

    signal gt_rst_all       : std_logic;
    signal gt_rst_tx        : std_logic;
    signal gt_rst_rx        : std_logic;
    signal gtwiz_reset_all  : std_logic;
    signal rx_serdes_rst    : std_logic;

    signal gtwiz_rst_tx_datapath : std_logic;
    signal gtwiz_rst_rx_datapath : std_logic;

    signal gtwiz_tx_rst_done : std_logic;
    signal gtwiz_rx_rst_done : std_logic;

    signal rx_rst_core      : std_logic;
    signal tx_rst_core      : std_logic;

    signal rxusrclk2        : std_logic;
    signal main_clk         : std_logic;
    signal txdata           : std_logic_vector(127 downto 0);
    signal rxdata           : std_logic_vector(127 downto 0);
    signal gtloopback       : std_logic;

    signal txunderflow      : std_logic;
    signal userreg          : std_logic_vector(31 downto 0);


begin

    gt_rx_out_clk <= rxusrclk2;
    gt_tx_out_clk <= main_clk;

    rst <= not rst_n;

    sfp_tx_disable  <= '0';
    rx_los  <= sfp_rx_los;
    mod_abs <= sfp_mod_abs;
    tx_fault <= sfp_tx_fault;

    axis_clk        <= main_clk;

    inst_reset_helper: entity ccn_eth_lib.ccn_ethernet_reset_wrapper
    port map(
        sys_reset                   => rst,
        dclk                        => free_100_clk,
        s_axi_aclk                  => axi_clk,
        ctl_gt_reset_all            => gt_rst_all,
        ctl_gt_tx_reset             => gt_rst_tx,
        ctl_gt_rx_reset             => gt_rst_rx,
        gtwiz_reset_tx_datapath     => tx_data_rst,
        gtwiz_reset_rx_datapath     => rx_data_rst,
        gt_txusrclk2                => main_clk,
        gt_rxusrclk2                => rxusrclk2,
        rx_core_clk                 => main_clk,
        gt_tx_reset_in              => gtwiz_tx_rst_done,
        gt_rx_reset_in              => gtwiz_rx_rst_done,
        tx_core_reset_in            => tx_rst,
        rx_core_reset_in            => rx_rst,
        tx_core_reset_out           => tx_rst_core,
        rx_core_reset_out           => rx_rst_core,
        rx_serdes_reset_out         => rx_serdes_rst,
        usr_tx_reset                => open,
        usr_rx_reset                => open,
        gtwiz_reset_all             => gtwiz_reset_all,
        gtwiz_reset_tx_datapath_out => gtwiz_rst_tx_datapath,
        gtwiz_reset_rx_datapath_out => gtwiz_rst_rx_datapath
    );


    -- GENRATE "mux" TO SELECT GOOD IP
    -- Very dumb, but I did not find anything smarter...

    G0:if G_IPNUM=0 generate
        inst_gtwizard:ccn_gtwizard_0
        PORT map(

            -- Reset
            gtwiz_reset_all_in(0)              => gtwiz_reset_all,
            gtwiz_userclk_rx_reset_in(0)       => rx_clk_rst,
            gtwiz_userclk_tx_reset_in(0)       => tx_clk_rst,
            gtwiz_reset_tx_done_out(0)         => gtwiz_tx_rst_done,
            gtwiz_reset_rx_done_out(0)         => gtwiz_rx_rst_done,
            gtwiz_reset_tx_datapath_in(0)      => gtwiz_rst_tx_datapath,
            gtwiz_reset_rx_datapath_in(0)      => gtwiz_rst_rx_datapath,

            -- Clocks
            gtwiz_userclk_rx_usrclk2_out(0)    => rxusrclk2,
            gtwiz_userclk_tx_usrclk2_out(0)    => main_clk,

            -- Free run clock
            gtwiz_reset_clk_freerun_in(0)      => free_100_clk,

            -- SFP
            gthrxn_in(0)                       => sfp_rxn,
            gthrxp_in(0)                       => sfp_rxp,
            gthtxn_out(0)                      => sfp_txn,
            gthtxp_out(0)                      => sfp_txp,

            -- QPLL Common
            qpll0clk_in(0)                     => qpll0clk_in,
            qpll0refclk_in(0)                  => qpll0refclk_in,
            gtwiz_reset_qpll0lock_in(0)        => qpll0lock_in,
            gtwiz_reset_qpll0reset_out(0)      => qpll0reset_out,

            -- SERDES
            txdata_in                          => txdata,
            rxdata_out                         => rxdata,
            rxgearboxslip_in(0)                => rxgearboxslip,
            rxdatavalid_out                    => rxdatavalid,
            rxheader_out                       => rxheader,
            rxheadervalid_out                  => rxheadervalid,
            txheader_in                        => txheader,

            -- GT
            loopback_in                        => gt_loopback,
            --loopback_in(0)                     => '0',
            --loopback_in(1)                     => gtloopback,
            --loopback_in(2)                     => '0',

            -- Status
            gtwiz_userclk_tx_active_out(0)     => tx_clk_active,
            gtwiz_userclk_rx_active_out(0)     => rx_clk_active,
            gtpowergood_out(0)                 => gt_powergood,
            gtwiz_reset_rx_cdr_stable_out(0)   => cdr_stable,

            -- Not used
            txsequence_in                      => (others => '0'),
            gtwiz_reset_tx_pll_and_datapath_in => "0",
            gtwiz_reset_rx_pll_and_datapath_in => "0",
            qpll1clk_in                        => "0",
            qpll1refclk_in                     => "0",
            gtwiz_userclk_tx_srcclk_out        => open,
            gtwiz_userclk_tx_usrclk_out        => open,
            gtwiz_userclk_rx_srcclk_out        => open,
            gtwiz_userclk_rx_usrclk_out        => open,
            rxpmaresetdone_out                 => open,
            rxprgdivresetdone_out              => open,
            rxstartofseq_out                   => open,
            txpmaresetdone_out                 => open,
            txprgdivresetdone_out              => open
        );

        inst_ethernet:ccn_eth_core_0
        PORT map(
            s_axi_aresetn_0                  => rst_n,

            -- GT reset
            rx_reset_0                       => rx_rst_core,
            tx_reset_0                       => tx_rst_core,
            gtwiz_reset_tx_done_0            => gtwiz_tx_rst_done,
            gtwiz_reset_rx_done_0            => gtwiz_rx_rst_done,
            ctl_gt_reset_all_0               => gt_rst_all,
            ctl_gt_tx_reset_0                => gt_rst_tx,
            ctl_gt_rx_reset_0                => gt_rst_rx,
            rx_serdes_reset_0                => rx_serdes_rst,

            -- Clocks
            rx_serdes_clk_0                  => rxusrclk2,
            rx_core_clk_0                    => main_clk,
            tx_core_clk_0                    => main_clk,

            -- SERDES
            rx_serdes_data_out_0             => rxdata,
            tx_serdes_data_in_0              => txdata,
            rxgearboxslip_in_0               => rxgearboxslip,
            rxdatavalid_out_0                => rxdatavalid,
            rxheader_out_0                   => rxheader,
            rxheadervalid_out_0              => rxheadervalid,
            txheader_in_0                    => txheader,

            -- GT
            gt_loopback_out_0                => gtloopback,

            -- AXI MM
            s_axi_aclk_0                     => axi_clk,
            s_axi_awaddr_0(11 downto 0)      => s_axi_awaddr,
            s_axi_awaddr_0(31 downto 12)     => (others => '0'),
            s_axi_awvalid_0                  => s_axi_awvalid,
            s_axi_awready_0                  => s_axi_awready,
            s_axi_wdata_0                    => s_axi_wdata,
            s_axi_wstrb_0                    => s_axi_wstrb,
            s_axi_wvalid_0                   => s_axi_wvalid,
            s_axi_wready_0                   => s_axi_wready,
            s_axi_bresp_0                    => s_axi_bresp,
            s_axi_bvalid_0                   => s_axi_bvalid,
            s_axi_bready_0                   => s_axi_bready,
            s_axi_araddr_0(11 downto 0)      => s_axi_araddr,
            s_axi_araddr_0(31 downto 12)     => (others => '0'),
            s_axi_arvalid_0                  => s_axi_arvalid,
            s_axi_arready_0                  => s_axi_arready,
            s_axi_rdata_0                    => s_axi_rdata,
            s_axi_rresp_0                    => s_axi_rresp,
            s_axi_rvalid_0                   => s_axi_rvalid,
            s_axi_rready_0                   => s_axi_rready,

            -- AXI STREAM
            rx_axis_tvalid_0                 => rx_axis_tvalid,
            rx_axis_tdata_0                  => rx_axis_tdata,
            rx_axis_tlast_0                  => rx_axis_tlast,
            rx_axis_tkeep_0                  => rx_axis_tkeep,
            rx_axis_tuser_0                  => rx_axis_tuser,
            tx_axis_tready_0                 => tx_axis_tready,
            tx_axis_tvalid_0                 => tx_axis_tvalid,
            tx_axis_tdata_0                  => tx_axis_tdata,
            tx_axis_tlast_0                  => tx_axis_tlast,
            tx_axis_tkeep_0                  => tx_axis_tkeep,
            tx_axis_tuser_0                  => tx_axis_tuser,

            -- Status
            tx_unfout_0                      => txunderflow,
            user_reg0_0                      => userreg,

            -- Stat latch tick
            pm_tick_0                        => pm_tick,

            -- Unused
            tx_preamblein_0                  => (others => '0'),
            rx_preambleout_0                 => open,
            ctl_tx_send_rfi_0                => '0',
            ctl_tx_send_lfi_0                => '0',
            ctl_tx_send_idle_0               => '0',

            -- Unused (stat)
            stat_rx_framing_err_0            => open,
            stat_rx_framing_err_valid_0      => open,
            stat_rx_local_fault_0            => open,
            stat_rx_block_lock_0             => open,
            stat_rx_valid_ctrl_code_0        => open,
            stat_rx_status_0                 => open,
            stat_rx_remote_fault_0           => open,
            stat_rx_bad_fcs_0                => open,
            stat_rx_stomped_fcs_0            => open,
            stat_rx_truncated_0              => open,
            stat_rx_internal_local_fault_0   => open,
            stat_rx_received_local_fault_0   => open,
            stat_rx_hi_ber_0                 => open,
            stat_rx_got_signal_os_0          => open,
            stat_rx_test_pattern_mismatch_0  => open,
            stat_rx_total_bytes_0            => open,
            stat_rx_total_packets_0          => open,
            stat_rx_total_good_bytes_0       => open,
            stat_rx_total_good_packets_0     => open,
            stat_rx_packet_bad_fcs_0         => open,
            stat_rx_packet_64_bytes_0        => open,
            stat_rx_packet_65_127_bytes_0    => open,
            stat_rx_packet_128_255_bytes_0   => open,
            stat_rx_packet_256_511_bytes_0   => open,
            stat_rx_packet_512_1023_bytes_0  => open,
            stat_rx_packet_1024_1518_bytes_0 => open,
            stat_rx_packet_1519_1522_bytes_0 => open,
            stat_rx_packet_1523_1548_bytes_0 => open,
            stat_rx_packet_1549_2047_bytes_0 => open,
            stat_rx_packet_2048_4095_bytes_0 => open,
            stat_rx_packet_4096_8191_bytes_0 => open,
            stat_rx_packet_8192_9215_bytes_0 => open,
            stat_rx_packet_small_0           => open,
            stat_rx_packet_large_0           => open,
            stat_rx_unicast_0                => open,
            stat_rx_multicast_0              => open,
            stat_rx_broadcast_0              => open,
            stat_rx_oversize_0               => open,
            stat_rx_toolong_0                => open,
            stat_rx_undersize_0              => open,
            stat_rx_fragment_0               => open,
            stat_rx_vlan_0                   => open,
            stat_rx_inrangeerr_0             => open,
            stat_rx_jabber_0                 => open,
            stat_rx_bad_code_0               => open,
            stat_rx_bad_sfd_0                => open,
            stat_rx_bad_preamble_0           => open,
            stat_tx_local_fault_0            => open,
            stat_tx_total_bytes_0            => open,
            stat_tx_total_packets_0          => open,
            stat_tx_total_good_bytes_0       => open,
            stat_tx_total_good_packets_0     => open,
            stat_tx_bad_fcs_0                => open,
            stat_tx_packet_64_bytes_0        => open,
            stat_tx_packet_65_127_bytes_0    => open,
            stat_tx_packet_128_255_bytes_0   => open,
            stat_tx_packet_256_511_bytes_0   => open,
            stat_tx_packet_512_1023_bytes_0  => open,
            stat_tx_packet_1024_1518_bytes_0 => open,
            stat_tx_packet_1519_1522_bytes_0 => open,
            stat_tx_packet_1523_1548_bytes_0 => open,
            stat_tx_packet_1549_2047_bytes_0 => open,
            stat_tx_packet_2048_4095_bytes_0 => open,
            stat_tx_packet_4096_8191_bytes_0 => open,
            stat_tx_packet_8192_9215_bytes_0 => open,
            stat_tx_packet_small_0           => open,
            stat_tx_packet_large_0           => open,
            stat_tx_unicast_0                => open,
            stat_tx_multicast_0              => open,
            stat_tx_broadcast_0              => open,
            stat_tx_vlan_0                   => open,
            stat_tx_frame_error_0            => open
        );

    end generate G0;


    G1:if G_IPNUM=1 generate
        inst_gtwizard:ccn_gtwizard_1
        PORT map(

            -- Reset
            gtwiz_reset_all_in(0)              => gtwiz_reset_all,
            gtwiz_userclk_rx_reset_in(0)       => rx_clk_rst,
            gtwiz_userclk_tx_reset_in(0)       => tx_clk_rst,
            gtwiz_reset_tx_done_out(0)         => gtwiz_tx_rst_done,
            gtwiz_reset_rx_done_out(0)         => gtwiz_rx_rst_done,
            gtwiz_reset_tx_datapath_in(0)      => gtwiz_rst_tx_datapath,
            gtwiz_reset_rx_datapath_in(0)      => gtwiz_rst_rx_datapath,

            -- Clocks
            gtwiz_userclk_rx_usrclk2_out(0)    => rxusrclk2,
            gtwiz_userclk_tx_usrclk2_out(0)    => main_clk,

            -- Free run clock
            gtwiz_reset_clk_freerun_in(0)      => free_100_clk,

            -- SFP
            gthrxn_in(0)                       => sfp_rxn,
            gthrxp_in(0)                       => sfp_rxp,
            gthtxn_out(0)                      => sfp_txn,
            gthtxp_out(0)                      => sfp_txp,

            -- QPLL Common
            qpll0clk_in(0)                     => qpll0clk_in,
            qpll0refclk_in(0)                  => qpll0refclk_in,
            gtwiz_reset_qpll0lock_in(0)        => qpll0lock_in,
            gtwiz_reset_qpll0reset_out(0)      => qpll0reset_out,

            -- SERDES
            txdata_in                          => txdata,
            rxdata_out                         => rxdata,
            rxgearboxslip_in(0)                => rxgearboxslip,
            rxdatavalid_out                    => rxdatavalid,
            rxheader_out                       => rxheader,
            rxheadervalid_out                  => rxheadervalid,
            txheader_in                        => txheader,

            -- GT
            loopback_in                        => gt_loopback,
            --loopback_in(0)                     => '0',
            --loopback_in(1)                     => gtloopback,
            --loopback_in(2)                     => '0',

            -- Status
            gtwiz_userclk_tx_active_out(0)     => tx_clk_active,
            gtwiz_userclk_rx_active_out(0)     => rx_clk_active,
            gtpowergood_out(0)                 => gt_powergood,
            gtwiz_reset_rx_cdr_stable_out(0)   => cdr_stable,

            -- Not used
            txsequence_in                      => (others => '0'),
            gtwiz_reset_tx_pll_and_datapath_in => "0",
            gtwiz_reset_rx_pll_and_datapath_in => "0",
            qpll1clk_in                        => "0",
            qpll1refclk_in                     => "0",
            gtwiz_userclk_tx_srcclk_out        => open,
            gtwiz_userclk_tx_usrclk_out        => open,
            gtwiz_userclk_rx_srcclk_out        => open,
            gtwiz_userclk_rx_usrclk_out        => open,
            rxpmaresetdone_out                 => open,
            rxprgdivresetdone_out              => open,
            rxstartofseq_out                   => open,
            txpmaresetdone_out                 => open,
            txprgdivresetdone_out              => open
        );

        inst_ethernet:ccn_eth_core_1
        PORT map(
            s_axi_aresetn_0                  => rst_n,

            -- GT reset
            rx_reset_0                       => rx_rst_core,
            tx_reset_0                       => tx_rst_core,
            gtwiz_reset_tx_done_0            => gtwiz_tx_rst_done,
            gtwiz_reset_rx_done_0            => gtwiz_rx_rst_done,
            ctl_gt_reset_all_0               => gt_rst_all,
            ctl_gt_tx_reset_0                => gt_rst_tx,
            ctl_gt_rx_reset_0                => gt_rst_rx,
            rx_serdes_reset_0                => rx_serdes_rst,

            -- Clocks
            rx_serdes_clk_0                  => rxusrclk2,
            rx_core_clk_0                    => main_clk,
            tx_core_clk_0                    => main_clk,

            -- SERDES
            rx_serdes_data_out_0             => rxdata,
            tx_serdes_data_in_0              => txdata,
            rxgearboxslip_in_0               => rxgearboxslip,
            rxdatavalid_out_0                => rxdatavalid,
            rxheader_out_0                   => rxheader,
            rxheadervalid_out_0              => rxheadervalid,
            txheader_in_0                    => txheader,

            -- GT
            gt_loopback_out_0                => gtloopback,

            -- AXI MM
            s_axi_aclk_0                     => axi_clk,
            s_axi_awaddr_0(11 downto 0)      => s_axi_awaddr,
            s_axi_awaddr_0(31 downto 12)     => (others => '0'),
            s_axi_awvalid_0                  => s_axi_awvalid,
            s_axi_awready_0                  => s_axi_awready,
            s_axi_wdata_0                    => s_axi_wdata,
            s_axi_wstrb_0                    => s_axi_wstrb,
            s_axi_wvalid_0                   => s_axi_wvalid,
            s_axi_wready_0                   => s_axi_wready,
            s_axi_bresp_0                    => s_axi_bresp,
            s_axi_bvalid_0                   => s_axi_bvalid,
            s_axi_bready_0                   => s_axi_bready,
            s_axi_araddr_0(11 downto 0)      => s_axi_araddr,
            s_axi_araddr_0(31 downto 12)     => (others => '0'),
            s_axi_arvalid_0                  => s_axi_arvalid,
            s_axi_arready_0                  => s_axi_arready,
            s_axi_rdata_0                    => s_axi_rdata,
            s_axi_rresp_0                    => s_axi_rresp,
            s_axi_rvalid_0                   => s_axi_rvalid,
            s_axi_rready_0                   => s_axi_rready,

            -- AXI STREAM
            rx_axis_tvalid_0                 => rx_axis_tvalid,
            rx_axis_tdata_0                  => rx_axis_tdata,
            rx_axis_tlast_0                  => rx_axis_tlast,
            rx_axis_tkeep_0                  => rx_axis_tkeep,
            rx_axis_tuser_0                  => rx_axis_tuser,
            tx_axis_tready_0                 => tx_axis_tready,
            tx_axis_tvalid_0                 => tx_axis_tvalid,
            tx_axis_tdata_0                  => tx_axis_tdata,
            tx_axis_tlast_0                  => tx_axis_tlast,
            tx_axis_tkeep_0                  => tx_axis_tkeep,
            tx_axis_tuser_0                  => tx_axis_tuser,

            -- Status
            tx_unfout_0                      => txunderflow,
            user_reg0_0                      => userreg,

            -- Stat latch tick
            pm_tick_0                        => pm_tick,

            -- Unused
            tx_preamblein_0                  => (others => '0'),
            rx_preambleout_0                 => open,
            ctl_tx_send_rfi_0                => '0',
            ctl_tx_send_lfi_0                => '0',
            ctl_tx_send_idle_0               => '0',

            -- Unused (stat)
            stat_rx_framing_err_0            => open,
            stat_rx_framing_err_valid_0      => open,
            stat_rx_local_fault_0            => open,
            stat_rx_block_lock_0             => open,
            stat_rx_valid_ctrl_code_0        => open,
            stat_rx_status_0                 => open,
            stat_rx_remote_fault_0           => open,
            stat_rx_bad_fcs_0                => open,
            stat_rx_stomped_fcs_0            => open,
            stat_rx_truncated_0              => open,
            stat_rx_internal_local_fault_0   => open,
            stat_rx_received_local_fault_0   => open,
            stat_rx_hi_ber_0                 => open,
            stat_rx_got_signal_os_0          => open,
            stat_rx_test_pattern_mismatch_0  => open,
            stat_rx_total_bytes_0            => open,
            stat_rx_total_packets_0          => open,
            stat_rx_total_good_bytes_0       => open,
            stat_rx_total_good_packets_0     => open,
            stat_rx_packet_bad_fcs_0         => open,
            stat_rx_packet_64_bytes_0        => open,
            stat_rx_packet_65_127_bytes_0    => open,
            stat_rx_packet_128_255_bytes_0   => open,
            stat_rx_packet_256_511_bytes_0   => open,
            stat_rx_packet_512_1023_bytes_0  => open,
            stat_rx_packet_1024_1518_bytes_0 => open,
            stat_rx_packet_1519_1522_bytes_0 => open,
            stat_rx_packet_1523_1548_bytes_0 => open,
            stat_rx_packet_1549_2047_bytes_0 => open,
            stat_rx_packet_2048_4095_bytes_0 => open,
            stat_rx_packet_4096_8191_bytes_0 => open,
            stat_rx_packet_8192_9215_bytes_0 => open,
            stat_rx_packet_small_0           => open,
            stat_rx_packet_large_0           => open,
            stat_rx_unicast_0                => open,
            stat_rx_multicast_0              => open,
            stat_rx_broadcast_0              => open,
            stat_rx_oversize_0               => open,
            stat_rx_toolong_0                => open,
            stat_rx_undersize_0              => open,
            stat_rx_fragment_0               => open,
            stat_rx_vlan_0                   => open,
            stat_rx_inrangeerr_0             => open,
            stat_rx_jabber_0                 => open,
            stat_rx_bad_code_0               => open,
            stat_rx_bad_sfd_0                => open,
            stat_rx_bad_preamble_0           => open,
            stat_tx_local_fault_0            => open,
            stat_tx_total_bytes_0            => open,
            stat_tx_total_packets_0          => open,
            stat_tx_total_good_bytes_0       => open,
            stat_tx_total_good_packets_0     => open,
            stat_tx_bad_fcs_0                => open,
            stat_tx_packet_64_bytes_0        => open,
            stat_tx_packet_65_127_bytes_0    => open,
            stat_tx_packet_128_255_bytes_0   => open,
            stat_tx_packet_256_511_bytes_0   => open,
            stat_tx_packet_512_1023_bytes_0  => open,
            stat_tx_packet_1024_1518_bytes_0 => open,
            stat_tx_packet_1519_1522_bytes_0 => open,
            stat_tx_packet_1523_1548_bytes_0 => open,
            stat_tx_packet_1549_2047_bytes_0 => open,
            stat_tx_packet_2048_4095_bytes_0 => open,
            stat_tx_packet_4096_8191_bytes_0 => open,
            stat_tx_packet_8192_9215_bytes_0 => open,
            stat_tx_packet_small_0           => open,
            stat_tx_packet_large_0           => open,
            stat_tx_unicast_0                => open,
            stat_tx_multicast_0              => open,
            stat_tx_broadcast_0              => open,
            stat_tx_vlan_0                   => open,
            stat_tx_frame_error_0            => open
        );

    end generate G1;

    G2:if G_IPNUM=2 generate
        inst_gtwizard:ccn_gtwizard_2
        PORT map(

            -- Reset
            gtwiz_reset_all_in(0)              => gtwiz_reset_all,
            gtwiz_userclk_rx_reset_in(0)       => rx_clk_rst,
            gtwiz_userclk_tx_reset_in(0)       => tx_clk_rst,
            gtwiz_reset_tx_done_out(0)         => gtwiz_tx_rst_done,
            gtwiz_reset_rx_done_out(0)         => gtwiz_rx_rst_done,
            gtwiz_reset_tx_datapath_in(0)      => gtwiz_rst_tx_datapath,
            gtwiz_reset_rx_datapath_in(0)      => gtwiz_rst_rx_datapath,

            -- Clocks
            gtwiz_userclk_rx_usrclk2_out(0)    => rxusrclk2,
            gtwiz_userclk_tx_usrclk2_out(0)    => main_clk,

            -- Free run clock
            gtwiz_reset_clk_freerun_in(0)      => free_100_clk,

            -- SFP
            gthrxn_in(0)                       => sfp_rxn,
            gthrxp_in(0)                       => sfp_rxp,
            gthtxn_out(0)                      => sfp_txn,
            gthtxp_out(0)                      => sfp_txp,

            -- QPLL Common
            qpll0clk_in(0)                     => qpll0clk_in,
            qpll0refclk_in(0)                  => qpll0refclk_in,
            gtwiz_reset_qpll0lock_in(0)        => qpll0lock_in,
            gtwiz_reset_qpll0reset_out(0)      => qpll0reset_out,

            -- SERDES
            txdata_in                          => txdata,
            rxdata_out                         => rxdata,
            rxgearboxslip_in(0)                => rxgearboxslip,
            rxdatavalid_out                    => rxdatavalid,
            rxheader_out                       => rxheader,
            rxheadervalid_out                  => rxheadervalid,
            txheader_in                        => txheader,

            -- GT
            loopback_in                        => gt_loopback,
            --loopback_in(0)                     => '0',
            --loopback_in(1)                     => gtloopback,
            --loopback_in(2)                     => '0',

            -- Status
            gtwiz_userclk_tx_active_out(0)     => tx_clk_active,
            gtwiz_userclk_rx_active_out(0)     => rx_clk_active,
            gtpowergood_out(0)                 => gt_powergood,
            gtwiz_reset_rx_cdr_stable_out(0)   => cdr_stable,

            -- Not used
            txsequence_in                      => (others => '0'),
            gtwiz_reset_tx_pll_and_datapath_in => "0",
            gtwiz_reset_rx_pll_and_datapath_in => "0",
            qpll1clk_in                        => "0",
            qpll1refclk_in                     => "0",
            gtwiz_userclk_tx_srcclk_out        => open,
            gtwiz_userclk_tx_usrclk_out        => open,
            gtwiz_userclk_rx_srcclk_out        => open,
            gtwiz_userclk_rx_usrclk_out        => open,
            rxpmaresetdone_out                 => open,
            rxprgdivresetdone_out              => open,
            rxstartofseq_out                   => open,
            txpmaresetdone_out                 => open,
            txprgdivresetdone_out              => open
        );

        inst_ethernet:ccn_eth_core_2
        PORT map(
            s_axi_aresetn_0                  => rst_n,

            -- GT reset
            rx_reset_0                       => rx_rst_core,
            tx_reset_0                       => tx_rst_core,
            gtwiz_reset_tx_done_0            => gtwiz_tx_rst_done,
            gtwiz_reset_rx_done_0            => gtwiz_rx_rst_done,
            ctl_gt_reset_all_0               => gt_rst_all,
            ctl_gt_tx_reset_0                => gt_rst_tx,
            ctl_gt_rx_reset_0                => gt_rst_rx,
            rx_serdes_reset_0                => rx_serdes_rst,

            -- Clocks
            rx_serdes_clk_0                  => rxusrclk2,
            rx_core_clk_0                    => main_clk,
            tx_core_clk_0                    => main_clk,

            -- SERDES
            rx_serdes_data_out_0             => rxdata,
            tx_serdes_data_in_0              => txdata,
            rxgearboxslip_in_0               => rxgearboxslip,
            rxdatavalid_out_0                => rxdatavalid,
            rxheader_out_0                   => rxheader,
            rxheadervalid_out_0              => rxheadervalid,
            txheader_in_0                    => txheader,

            -- GT
            gt_loopback_out_0                => gtloopback,

            -- AXI MM
            s_axi_aclk_0                     => axi_clk,
            s_axi_awaddr_0(11 downto 0)      => s_axi_awaddr,
            s_axi_awaddr_0(31 downto 12)     => (others => '0'),
            s_axi_awvalid_0                  => s_axi_awvalid,
            s_axi_awready_0                  => s_axi_awready,
            s_axi_wdata_0                    => s_axi_wdata,
            s_axi_wstrb_0                    => s_axi_wstrb,
            s_axi_wvalid_0                   => s_axi_wvalid,
            s_axi_wready_0                   => s_axi_wready,
            s_axi_bresp_0                    => s_axi_bresp,
            s_axi_bvalid_0                   => s_axi_bvalid,
            s_axi_bready_0                   => s_axi_bready,
            s_axi_araddr_0(11 downto 0)      => s_axi_araddr,
            s_axi_araddr_0(31 downto 12)     => (others => '0'),
            s_axi_arvalid_0                  => s_axi_arvalid,
            s_axi_arready_0                  => s_axi_arready,
            s_axi_rdata_0                    => s_axi_rdata,
            s_axi_rresp_0                    => s_axi_rresp,
            s_axi_rvalid_0                   => s_axi_rvalid,
            s_axi_rready_0                   => s_axi_rready,

            -- AXI STREAM
            rx_axis_tvalid_0                 => rx_axis_tvalid,
            rx_axis_tdata_0                  => rx_axis_tdata,
            rx_axis_tlast_0                  => rx_axis_tlast,
            rx_axis_tkeep_0                  => rx_axis_tkeep,
            rx_axis_tuser_0                  => rx_axis_tuser,
            tx_axis_tready_0                 => tx_axis_tready,
            tx_axis_tvalid_0                 => tx_axis_tvalid,
            tx_axis_tdata_0                  => tx_axis_tdata,
            tx_axis_tlast_0                  => tx_axis_tlast,
            tx_axis_tkeep_0                  => tx_axis_tkeep,
            tx_axis_tuser_0                  => tx_axis_tuser,

            -- Status
            tx_unfout_0                      => txunderflow,
            user_reg0_0                      => userreg,

            -- Stat latch tick
            pm_tick_0                        => pm_tick,

            -- Unused
            tx_preamblein_0                  => (others => '0'),
            rx_preambleout_0                 => open,
            ctl_tx_send_rfi_0                => '0',
            ctl_tx_send_lfi_0                => '0',
            ctl_tx_send_idle_0               => '0',

            -- Unused (stat)
            stat_rx_framing_err_0            => open,
            stat_rx_framing_err_valid_0      => open,
            stat_rx_local_fault_0            => open,
            stat_rx_block_lock_0             => open,
            stat_rx_valid_ctrl_code_0        => open,
            stat_rx_status_0                 => open,
            stat_rx_remote_fault_0           => open,
            stat_rx_bad_fcs_0                => open,
            stat_rx_stomped_fcs_0            => open,
            stat_rx_truncated_0              => open,
            stat_rx_internal_local_fault_0   => open,
            stat_rx_received_local_fault_0   => open,
            stat_rx_hi_ber_0                 => open,
            stat_rx_got_signal_os_0          => open,
            stat_rx_test_pattern_mismatch_0  => open,
            stat_rx_total_bytes_0            => open,
            stat_rx_total_packets_0          => open,
            stat_rx_total_good_bytes_0       => open,
            stat_rx_total_good_packets_0     => open,
            stat_rx_packet_bad_fcs_0         => open,
            stat_rx_packet_64_bytes_0        => open,
            stat_rx_packet_65_127_bytes_0    => open,
            stat_rx_packet_128_255_bytes_0   => open,
            stat_rx_packet_256_511_bytes_0   => open,
            stat_rx_packet_512_1023_bytes_0  => open,
            stat_rx_packet_1024_1518_bytes_0 => open,
            stat_rx_packet_1519_1522_bytes_0 => open,
            stat_rx_packet_1523_1548_bytes_0 => open,
            stat_rx_packet_1549_2047_bytes_0 => open,
            stat_rx_packet_2048_4095_bytes_0 => open,
            stat_rx_packet_4096_8191_bytes_0 => open,
            stat_rx_packet_8192_9215_bytes_0 => open,
            stat_rx_packet_small_0           => open,
            stat_rx_packet_large_0           => open,
            stat_rx_unicast_0                => open,
            stat_rx_multicast_0              => open,
            stat_rx_broadcast_0              => open,
            stat_rx_oversize_0               => open,
            stat_rx_toolong_0                => open,
            stat_rx_undersize_0              => open,
            stat_rx_fragment_0               => open,
            stat_rx_vlan_0                   => open,
            stat_rx_inrangeerr_0             => open,
            stat_rx_jabber_0                 => open,
            stat_rx_bad_code_0               => open,
            stat_rx_bad_sfd_0                => open,
            stat_rx_bad_preamble_0           => open,
            stat_tx_local_fault_0            => open,
            stat_tx_total_bytes_0            => open,
            stat_tx_total_packets_0          => open,
            stat_tx_total_good_bytes_0       => open,
            stat_tx_total_good_packets_0     => open,
            stat_tx_bad_fcs_0                => open,
            stat_tx_packet_64_bytes_0        => open,
            stat_tx_packet_65_127_bytes_0    => open,
            stat_tx_packet_128_255_bytes_0   => open,
            stat_tx_packet_256_511_bytes_0   => open,
            stat_tx_packet_512_1023_bytes_0  => open,
            stat_tx_packet_1024_1518_bytes_0 => open,
            stat_tx_packet_1519_1522_bytes_0 => open,
            stat_tx_packet_1523_1548_bytes_0 => open,
            stat_tx_packet_1549_2047_bytes_0 => open,
            stat_tx_packet_2048_4095_bytes_0 => open,
            stat_tx_packet_4096_8191_bytes_0 => open,
            stat_tx_packet_8192_9215_bytes_0 => open,
            stat_tx_packet_small_0           => open,
            stat_tx_packet_large_0           => open,
            stat_tx_unicast_0                => open,
            stat_tx_multicast_0              => open,
            stat_tx_broadcast_0              => open,
            stat_tx_vlan_0                   => open,
            stat_tx_frame_error_0            => open
        );

    end generate G2;

    G3:if G_IPNUM=3 generate
        inst_gtwizard:ccn_gtwizard_3
        PORT map(

            -- Reset
            gtwiz_reset_all_in(0)              => gtwiz_reset_all,
            gtwiz_userclk_rx_reset_in(0)       => rx_clk_rst,
            gtwiz_userclk_tx_reset_in(0)       => tx_clk_rst,
            gtwiz_reset_tx_done_out(0)         => gtwiz_tx_rst_done,
            gtwiz_reset_rx_done_out(0)         => gtwiz_rx_rst_done,
            gtwiz_reset_tx_datapath_in(0)      => gtwiz_rst_tx_datapath,
            gtwiz_reset_rx_datapath_in(0)      => gtwiz_rst_rx_datapath,

            -- Clocks
            gtwiz_userclk_rx_usrclk2_out(0)    => rxusrclk2,
            gtwiz_userclk_tx_usrclk2_out(0)    => main_clk,

            -- Free run clock
            gtwiz_reset_clk_freerun_in(0)      => free_100_clk,

            -- SFP
            gthrxn_in(0)                       => sfp_rxn,
            gthrxp_in(0)                       => sfp_rxp,
            gthtxn_out(0)                      => sfp_txn,
            gthtxp_out(0)                      => sfp_txp,

            -- QPLL Common
            qpll0clk_in(0)                     => qpll0clk_in,
            qpll0refclk_in(0)                  => qpll0refclk_in,
            gtwiz_reset_qpll0lock_in(0)        => qpll0lock_in,
            gtwiz_reset_qpll0reset_out(0)      => qpll0reset_out,

            -- SERDES
            txdata_in                          => txdata,
            rxdata_out                         => rxdata,
            rxgearboxslip_in(0)                => rxgearboxslip,
            rxdatavalid_out                    => rxdatavalid,
            rxheader_out                       => rxheader,
            rxheadervalid_out                  => rxheadervalid,
            txheader_in                        => txheader,

            -- GT
            loopback_in                        => gt_loopback,
            --loopback_in(0)                     => '0',
            --loopback_in(1)                     => gtloopback,
            --loopback_in(2)                     => '0',

            -- Status
            gtwiz_userclk_tx_active_out(0)     => tx_clk_active,
            gtwiz_userclk_rx_active_out(0)     => rx_clk_active,
            gtpowergood_out(0)                 => gt_powergood,
            gtwiz_reset_rx_cdr_stable_out(0)   => cdr_stable,

            -- Not used
            txsequence_in                      => (others => '0'),
            gtwiz_reset_tx_pll_and_datapath_in => "0",
            gtwiz_reset_rx_pll_and_datapath_in => "0",
            qpll1clk_in                        => "0",
            qpll1refclk_in                     => "0",
            gtwiz_userclk_tx_srcclk_out        => open,
            gtwiz_userclk_tx_usrclk_out        => open,
            gtwiz_userclk_rx_srcclk_out        => open,
            gtwiz_userclk_rx_usrclk_out        => open,
            rxpmaresetdone_out                 => open,
            rxprgdivresetdone_out              => open,
            rxstartofseq_out                   => open,
            txpmaresetdone_out                 => open,
            txprgdivresetdone_out              => open
        );

        inst_ethernet:ccn_eth_core_3
        PORT map(
            s_axi_aresetn_0                  => rst_n,

            -- GT reset
            rx_reset_0                       => rx_rst_core,
            tx_reset_0                       => tx_rst_core,
            gtwiz_reset_tx_done_0            => gtwiz_tx_rst_done,
            gtwiz_reset_rx_done_0            => gtwiz_rx_rst_done,
            ctl_gt_reset_all_0               => gt_rst_all,
            ctl_gt_tx_reset_0                => gt_rst_tx,
            ctl_gt_rx_reset_0                => gt_rst_rx,
            rx_serdes_reset_0                => rx_serdes_rst,

            -- Clocks
            rx_serdes_clk_0                  => rxusrclk2,
            rx_core_clk_0                    => main_clk,
            tx_core_clk_0                    => main_clk,

            -- SERDES
            rx_serdes_data_out_0             => rxdata,
            tx_serdes_data_in_0              => txdata,
            rxgearboxslip_in_0               => rxgearboxslip,
            rxdatavalid_out_0                => rxdatavalid,
            rxheader_out_0                   => rxheader,
            rxheadervalid_out_0              => rxheadervalid,
            txheader_in_0                    => txheader,

            -- GT
            gt_loopback_out_0                => gtloopback,

            -- AXI MM
            s_axi_aclk_0                     => axi_clk,
            s_axi_awaddr_0(11 downto 0)      => s_axi_awaddr,
            s_axi_awaddr_0(31 downto 12)     => (others => '0'),
            s_axi_awvalid_0                  => s_axi_awvalid,
            s_axi_awready_0                  => s_axi_awready,
            s_axi_wdata_0                    => s_axi_wdata,
            s_axi_wstrb_0                    => s_axi_wstrb,
            s_axi_wvalid_0                   => s_axi_wvalid,
            s_axi_wready_0                   => s_axi_wready,
            s_axi_bresp_0                    => s_axi_bresp,
            s_axi_bvalid_0                   => s_axi_bvalid,
            s_axi_bready_0                   => s_axi_bready,
            s_axi_araddr_0(11 downto 0)      => s_axi_araddr,
            s_axi_araddr_0(31 downto 12)     => (others => '0'),
            s_axi_arvalid_0                  => s_axi_arvalid,
            s_axi_arready_0                  => s_axi_arready,
            s_axi_rdata_0                    => s_axi_rdata,
            s_axi_rresp_0                    => s_axi_rresp,
            s_axi_rvalid_0                   => s_axi_rvalid,
            s_axi_rready_0                   => s_axi_rready,

            -- AXI STREAM
            rx_axis_tvalid_0                 => rx_axis_tvalid,
            rx_axis_tdata_0                  => rx_axis_tdata,
            rx_axis_tlast_0                  => rx_axis_tlast,
            rx_axis_tkeep_0                  => rx_axis_tkeep,
            rx_axis_tuser_0                  => rx_axis_tuser,
            tx_axis_tready_0                 => tx_axis_tready,
            tx_axis_tvalid_0                 => tx_axis_tvalid,
            tx_axis_tdata_0                  => tx_axis_tdata,
            tx_axis_tlast_0                  => tx_axis_tlast,
            tx_axis_tkeep_0                  => tx_axis_tkeep,
            tx_axis_tuser_0                  => tx_axis_tuser,

            -- Status
            tx_unfout_0                      => txunderflow,
            user_reg0_0                      => userreg,

            -- Stat latch tick
            pm_tick_0                        => pm_tick,

            -- Unused
            tx_preamblein_0                  => (others => '0'),
            rx_preambleout_0                 => open,
            ctl_tx_send_rfi_0                => '0',
            ctl_tx_send_lfi_0                => '0',
            ctl_tx_send_idle_0               => '0',

            -- Unused (stat)
            stat_rx_framing_err_0            => open,
            stat_rx_framing_err_valid_0      => open,
            stat_rx_local_fault_0            => open,
            stat_rx_block_lock_0             => open,
            stat_rx_valid_ctrl_code_0        => open,
            stat_rx_status_0                 => open,
            stat_rx_remote_fault_0           => open,
            stat_rx_bad_fcs_0                => open,
            stat_rx_stomped_fcs_0            => open,
            stat_rx_truncated_0              => open,
            stat_rx_internal_local_fault_0   => open,
            stat_rx_received_local_fault_0   => open,
            stat_rx_hi_ber_0                 => open,
            stat_rx_got_signal_os_0          => open,
            stat_rx_test_pattern_mismatch_0  => open,
            stat_rx_total_bytes_0            => open,
            stat_rx_total_packets_0          => open,
            stat_rx_total_good_bytes_0       => open,
            stat_rx_total_good_packets_0     => open,
            stat_rx_packet_bad_fcs_0         => open,
            stat_rx_packet_64_bytes_0        => open,
            stat_rx_packet_65_127_bytes_0    => open,
            stat_rx_packet_128_255_bytes_0   => open,
            stat_rx_packet_256_511_bytes_0   => open,
            stat_rx_packet_512_1023_bytes_0  => open,
            stat_rx_packet_1024_1518_bytes_0 => open,
            stat_rx_packet_1519_1522_bytes_0 => open,
            stat_rx_packet_1523_1548_bytes_0 => open,
            stat_rx_packet_1549_2047_bytes_0 => open,
            stat_rx_packet_2048_4095_bytes_0 => open,
            stat_rx_packet_4096_8191_bytes_0 => open,
            stat_rx_packet_8192_9215_bytes_0 => open,
            stat_rx_packet_small_0           => open,
            stat_rx_packet_large_0           => open,
            stat_rx_unicast_0                => open,
            stat_rx_multicast_0              => open,
            stat_rx_broadcast_0              => open,
            stat_rx_oversize_0               => open,
            stat_rx_toolong_0                => open,
            stat_rx_undersize_0              => open,
            stat_rx_fragment_0               => open,
            stat_rx_vlan_0                   => open,
            stat_rx_inrangeerr_0             => open,
            stat_rx_jabber_0                 => open,
            stat_rx_bad_code_0               => open,
            stat_rx_bad_sfd_0                => open,
            stat_rx_bad_preamble_0           => open,
            stat_tx_local_fault_0            => open,
            stat_tx_total_bytes_0            => open,
            stat_tx_total_packets_0          => open,
            stat_tx_total_good_bytes_0       => open,
            stat_tx_total_good_packets_0     => open,
            stat_tx_bad_fcs_0                => open,
            stat_tx_packet_64_bytes_0        => open,
            stat_tx_packet_65_127_bytes_0    => open,
            stat_tx_packet_128_255_bytes_0   => open,
            stat_tx_packet_256_511_bytes_0   => open,
            stat_tx_packet_512_1023_bytes_0  => open,
            stat_tx_packet_1024_1518_bytes_0 => open,
            stat_tx_packet_1519_1522_bytes_0 => open,
            stat_tx_packet_1523_1548_bytes_0 => open,
            stat_tx_packet_1549_2047_bytes_0 => open,
            stat_tx_packet_2048_4095_bytes_0 => open,
            stat_tx_packet_4096_8191_bytes_0 => open,
            stat_tx_packet_8192_9215_bytes_0 => open,
            stat_tx_packet_small_0           => open,
            stat_tx_packet_large_0           => open,
            stat_tx_unicast_0                => open,
            stat_tx_multicast_0              => open,
            stat_tx_broadcast_0              => open,
            stat_tx_vlan_0                   => open,
            stat_tx_frame_error_0            => open
        );

    end generate G3;

end architecture struct;
