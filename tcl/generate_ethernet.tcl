set ProjectDirPath [file join ${::fwfwk::PrjBuildPath} ${::fwfwk::PrjBuildName}]

## ------------------------------------- ##
## ETHERNET CORE
## ------------------------------------- ##
set ipName "ccn_eth_core_$idx"

set xcipath [create_ip \
    -name xxv_ethernet \
    -vendor xilinx.com \
    -library ip -version 3.3 \
    -module_name $ipName]

# These parameters are "hard coded" for our application:
# GT_TYPE : GTH (we are on FMC2 slot)
set ipProp [list \
    CONFIG.CORE {Ethernet MAC+PCS/PMA 64-bit} \
    CONFIG.LINE_RATE {10} \
    CONFIG.NUM_OF_CORES 1 \
    CONFIG.DATA_PATH_INTERFACE {AXI Stream} \
    CONFIG.BASE_R_KR {BASE-R} \
    CONFIG.INCLUDE_AXI4_INTERFACE {1} \
    CONFIG.INCLUDE_STATISTICS_COUNTERS {1} \
    CONFIG.INCLUDE_USER_FIFO {1} \
    CONFIG.ENABLE_TX_FLOW_CONTROL_LOGIC {0} \
    CONFIG.ENABLE_DATAPATH_PARITY {0} \
    CONFIG.GT_LOCATION {0} \
    CONFIG.GT_REF_CLK_FREQ {156.25} \
    CONFIG.GT_TYPE {GTH} \
    CONFIG.ENABLE_PIPELINE_REG {1} \
    CONFIG.ADD_GT_CNTRL_STS_PORTS {1} \
    CONFIG.INCLUDE_SHARED_LOGIC {0} \
    CONFIG.LANE1_GT_LOC $GTH_LOC \
    ]

set_property -dict $ipProp [get_ips $ipName]

generate_target all [get_files  ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]
export_ip_user_files -of_objects [get_files ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]

## ------------------------------------- ##
## GT WIZARD
## ------------------------------------- ##
set ipName "ccn_gtwizard_$idx"

set xcipath [create_ip \
    -name gtwizard_ultrascale \
    -vendor xilinx.com \
    -library ip -version 1.7 \
    -module_name $ipName]

# We need to put first the preset, then customize
set_property -dict [list \
    CONFIG.preset {GTH-10GBASE-R} \
    ] [get_ips $ipName]

# These parameters are "hard coded" for our application:
# RX_REFCLK_SOURCE: clk1 (clocks swapped on DAMCFMC2ZUP)
set ipProp [list \
    CONFIG.CHANNEL_ENABLE $GTH_LOC \
    CONFIG.TX_MASTER_CHANNEL $GTH_LOC \
    CONFIG.RX_MASTER_CHANNEL $GTH_LOC \
    CONFIG.FREERUN_FREQUENCY {100} \
    CONFIG.LOCATE_COMMON {EXAMPLE_DESIGN} \
    CONFIG.LOCATE_TX_USER_CLOCKING {CORE} \
    CONFIG.LOCATE_RX_USER_CLOCKING {CORE} \
    CONFIG.LOCATE_RESET_CONTROLLER {CORE} \
    CONFIG.LOCATE_USER_DATA_WIDTH_SIZING {EXAMPLE_DESIGN} \
    CONFIG.RX_USER_DATA_WIDTH 64 \
    CONFIG.TX_USER_DATA_WIDTH 64 \
    CONFIG.ENABLE_OPTIONAL_PORTS {loopback_in} \
    CONFIG.RX_REFCLK_SOURCE "${GTH_LOC} clk1" \
    CONFIG.TX_REFCLK_SOURCE "${GTH_LOC} clk1" \
    ]

set_property -dict $ipProp [get_ips $ipName]

generate_target all [get_files  ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]
export_ip_user_files -of_objects [get_files ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]


