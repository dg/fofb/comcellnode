################################################################################
# Main tcl for the module
################################################################################

# ==============================================================================
proc init {} {

    # Configuration
    variable Config

}

# ==============================================================================
proc setSources {} {
    variable Sources

    # Generate VHDL package with modle version
    genModVerFile VHDL ../hdl/pkg_ccn_ethernet_version.vhd

    # {SrcFile SrcType srcLib usedIn procOrder}
    lappend Sources {"../hdl/top_ccn_ethernet.vhd"          "VHDL"      "ccn_eth_lib"}
    lappend Sources {"../hdl/ccn_ethernet.vhd"              "VHDL"      "ccn_eth_lib"}
    lappend Sources {"../hdl/pkg_ccn_ethernet.vhd"          "VHDL"      "ccn_eth_lib"}
    lappend Sources {"../hdl/pkg_ccn_ethernet_version.vhd"  "VHDL"      "ccn_eth_lib"}
    lappend Sources {"../hdl/ccn_ethernet_reset_wrapper.v"  "verilog"   "ccn_eth_lib"}

}

# ==============================================================================
proc setAddressSpace {} {
    variable AddressSpace
    addAddressSpace AddressSpace "ccn_ethernet" RDL {} ../rdl/ccn_ethernet.rdl
    addAddressSpace AddressSpace "xilinx_ethsubsyst" RDL {} ../rdl/xilinx_ethsubsyst.rdl
}

# ==============================================================================
proc doOnCreate {} {
  variable Sources
  variable Config

  addSources Sources

  set idx 0
  foreach GTH_LOC $Config(GTH_LOC) {
      source generate_ethernet.tcl
      incr idx
  }
}

# ==============================================================================
proc doOnBuild {} {
}

# ==============================================================================
proc setSim {} {
}
